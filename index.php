<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<title>S01: PHP Basics and Selection Control structure</title>
</head>

<body>

<h1>Full Address</h1>
<p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswym Bldg., Timog Avenue'); ?></p>
<p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue'); ?></p>

<h1>Letter-Based Grading</h1>
<p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
<p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
<p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>



</body>
</html>