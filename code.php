<?php 

function getFullAddress($country, $city, $province, $specificAddress){

	return "$specificAddress, $city, $province, $country";
}



function getLetterGrade($grade) {
    if ($grade >= 98 && $grade <= 100) {
        return 'A+';
    } elseif ($grade >= 95 && $grade <= 97) {
        return 'A';
    } elseif ($grade >= 92 && $grade <= 94) {
        return 'A-';
    } elseif ($grade >= 89 && $grade <= 91) {
        return 'B+';
    } elseif ($grade >= 86 && $grade <= 88) {
        return 'B';
    } elseif ($grade >= 83 && $grade <= 85) {
        return 'B-';
    } elseif ($grade >= 80 && $grade <= 82) {
        return 'C+';
    } elseif ($grade >= 77 && $grade <= 79) {
        return 'C';
    } elseif ($grade >= 75 && $grade <= 76) {
        return 'C-';
    } elseif ($grade < 75) {
        return 'D';
    } else {
        return 'Invalid Grade';
    }
}


?>